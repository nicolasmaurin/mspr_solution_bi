#!/bin/bash

mysql -u "root" "-proot" <<EOF

CREATE DATABASE IF NOT EXISTS so_emballage;
CREATE USER IF NOT EXISTS 'so_emballage' IDENTIFIED BY 'so_emballage';
GRANT ALL PRIVILEGES ON so_emballage.* TO 'so_emballage'@'%';
FLUSH PRIVILEGES;

EOF

mysql -u "root" "-proot" "so_emballage" < ./crebas_SO-Emballage.sql
#mysql -u "root" "-proot" "so_emballage" < ./testdata_SO-Emballage.sql
