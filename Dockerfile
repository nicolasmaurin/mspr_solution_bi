FROM oracle/database:18.4.0-xe


COPY deployDb.sh /usr/local/bin/deployDb.sh
RUN chmod +x /usr/local/bin/deployDb.sh

CMD ["/bin/bash", "/usr/local/bin/deployDb.sh"]